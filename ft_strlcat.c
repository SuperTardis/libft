/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 18:23:10 by pporechn          #+#    #+#             */
/*   Updated: 2016/12/02 13:53:24 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *restrict des, const char *restrict s, size_t num)
{
	char		*dest;
	const char	*str;
	size_t		n;
	size_t		i;

	dest = des;
	str = s;
	n = num;
	while (n-- != 0 && *dest != '\0')
		dest++;
	i = dest - des;
	n = num - i;
	if (n == 0)
		return (i + ft_strlen(str));
	while (*str != '\0')
	{
		if (n != 1)
		{
			*dest++ = *str;
			n--;
		}
		str++;
	}
	*dest = '\0';
	return (i + (str - s));
}
