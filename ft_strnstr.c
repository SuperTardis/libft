/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 19:02:45 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 15:32:23 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s, const char *s1, size_t size)
{
	size_t		i;
	size_t		j;
	const char	*st1;
	const char	*st2;

	i = 0;
	if (!*s1)
		return ((char*)s);
	while (*s && i < size)
	{
		st1 = s;
		st2 = s1;
		j = i;
		while (*st1 && *st2 && !(*st1 - *st2) && j < size)
		{
			st1++;
			st2++;
			j++;
		}
		if (!*st2)
			return ((char*)s);
		s++;
		i++;
	}
	return (0);
}
