/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 17:04:44 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 13:05:55 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *des, const void *src, size_t n)
{
	char *d;
	char *s;

	if (n)
	{
		s = (char*)src;
		d = (char*)des;
		if (s < d)
		{
			s += n;
			d += n;
			while (n > 0)
			{
				*--d = *--s;
				n--;
			}
		}
		else
			while (n--)
				*d++ = *s++;
	}
	return (des);
}
