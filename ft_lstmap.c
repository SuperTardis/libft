/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 14:33:52 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 16:14:33 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*ntmp;
	t_list	*nls;
	t_list	*tmp1;
	t_list	*tmp2;

	tmp1 = lst;
	nls = (t_list*)malloc(sizeof(t_list));
	ntmp = nls;
	while (tmp1)
	{
		tmp2 = f((t_list*)tmp1);
		ntmp->content = tmp2->content;
		ntmp->content_size = tmp2->content_size;
		if (tmp1->next == NULL)
			break ;
		ntmp->next = (t_list*)malloc(sizeof(t_list));
		ntmp = ntmp->next;
		ntmp->next = NULL;
		tmp1 = tmp1->next;
	}
	return (nls);
}
