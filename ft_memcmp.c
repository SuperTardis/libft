/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 17:30:51 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 13:05:02 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char ss1;
	unsigned char ss2;

	while (n-- > 0)
	{
		ss1 = *(unsigned char*)s1;
		ss2 = *(unsigned char*)s2;
		if (ss1 != ss2)
			return (ss1 - ss2);
		s1++;
		s2++;
	}
	return (0);
}
