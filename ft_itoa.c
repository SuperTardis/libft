/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 19:44:52 by pporechn          #+#    #+#             */
/*   Updated: 2016/12/02 17:58:24 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_m(int n, int len)
{
	int		i;

	if (len <= 0)
		return (1);
	i = 1;
	while (len--)
		i *= n;
	return (i);
}

static int	ft_size(int n)
{
	int		s;

	s = 0;
	while (n)
	{
		s++;
		n /= 10;
	}
	return (s);
}

char		*ft_itoa(int n)
{
	int		len;
	char	*c;
	int		i;

	len = ft_size(n);
	if (n <= 0)
		c = (char*)malloc(sizeof(char) * (len + 2));
	else
		c = (char*)malloc(sizeof(char) * (len + 1));
	if (!c)
		return (0);
	i = 0;
	if (n < 0 && ++i)
		c[0] = '-';
	if (n == 0 && ++i)
		c[0] = 48;
	while (--len >= 0)
	{
		if (n < 0)
			c[i++] = 48 - (n / ft_m(10, len)) % 10;
		else
			c[i++] = 48 + (n / ft_m(10, len)) % 10;
	}
	c[i] = 0;
	return (c);
}
