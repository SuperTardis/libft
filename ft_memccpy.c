/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 15:45:40 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 13:00:46 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *des, const void *src, int c, size_t k)
{
	char	*s2;
	char	*s1;
	int		i;

	i = 0;
	s2 = (char*)src;
	s1 = (char*)des;
	while (k)
	{
		s1[i] = s2[i];
		if (s2[i] == c)
			return (&s1[i + 1]);
		k--;
		i++;
	}
	return (NULL);
}
