/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 11:48:02 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 13:13:51 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *str)
{
	int		i;
	int		size;
	char	*dup;

	i = 0;
	size = ft_strlen((char*)str);
	if (!(dup = (char*)malloc((size + 1) * sizeof(*str))))
		return (NULL);
	dup[size + 1] = '\0';
	while (i <= size)
	{
		dup[i] = str[i];
		i++;
	}
	return (dup);
}
