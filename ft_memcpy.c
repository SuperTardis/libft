/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pporechn <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 15:34:54 by pporechn          #+#    #+#             */
/*   Updated: 2016/11/28 15:34:56 by pporechn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *s, size_t n)
{
	unsigned char		*c;
	unsigned char		*c2;

	c = (unsigned char*)s;
	c2 = (unsigned char*)dest;
	while (n)
	{
		*c2++ = *c++;
		n--;
	}
	return (dest);
}
